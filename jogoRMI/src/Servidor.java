
/**
 * Interface Servidor.
 * Interace que define o servidor do jogo.
 * @author Fabio Junior e Douglas
 * @since 15/12/2015
 * @version 2.0
 */

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Servidor extends Remote {
	
	final static int STATUS_JOGO_AGUARDANDO = 1;
    final static int STATUS_JOGO_ATIVO = 2;    
    final static int STATUS_JOGO_FINALIZADO = 3;
	
    /**
     * Registra um cliente no servidor.
     * @param client
     * @throws RemoteException
     */
    public void registry(Cliente client) throws RemoteException;					
    
    /**
     * Obtem o status do jogo.
     * 1 - Jogo Aguardando (ainda nao comecou)
     * 2 - Jogo Ativo (em andamento)
     * 3 - Jogo Finalizado
     * @return status do jogo.
     * @throws RemoteException
     */
    public int getStatus() throws RemoteException;									
    
    /**
     * Define o status do jogo.
     * 1 - Jogo Aguardando (ainda nao comecou)
     * 2 - Jogo Ativo (em andamento)
     * 3 - Jogo Finalizado
     * @param status
     * @throws RemoteException
     */
    public void setStatus(int status) throws RemoteException;						
    
    /**
     * Executa o comando enviado pelo jogador.
     * @param command
     * @throws RemoteException
     */
    public void runCommand(String command) throws RemoteException;	
    
    /**
     * Obtem o numero de jogadores.
     * @return numero de jogadores.
     * @throws RemoteException
     */
    public int getPlayersNumber() throws RemoteException;							
    
    /**
     * Define o numero de jogadores.
     * @param number
     * @throws RemoteException
     */
    public void setPlayersNumber(int number) throws RemoteException;    			
    
    /**
     * Distribui as cartas aos jogadores.
     * @param clientID
     * @throws RemoteException
     */
    public void dealCards() throws RemoteException;									
    
    /**
     * Obtem o id do jogador da vez
     * @return id do jogador da vez
     * @throws RemoteException
     */
    public int getTurnPlayer() throws RemoteException;								
    
    /**
     * Define o proximo jogador a jogar
     * @param playerID
     * @throws RemoteException
     */
    public void setTurnPlayer(int playerID) throws RemoteException;
    
    /**
     * Obtem a carta descartada.
     * @return carta descartada.
     * @throws RemoteException
     */
    public String getDiscard() throws RemoteException;								
    
    /**
     * Define a carta descartada.
     * @param discard
     * @throws RemoteException
     */
    public void setDiscard(String discard) throws RemoteException;					   
    
    /**
     * Divulga a todos os jogadores quem foi o ganhador
     * @param winner
     * @throws RemoteException
     */
    public void setWinner(int playerID) throws RemoteException;	
    
    /**
     * Divulga a todos os jogadores quem foi o perdedor
     * @param winner
     * @throws RemoteException
     */
    public void setLoser(int playerID) throws RemoteException;	
    
    /**
     * Aumenta o numero de batedores.
     * @throws RemoteException
     */
    public void increaseDroppedNumber() throws RemoteException;    

    /**
     * Obtem o numero de batedores
     * @return numero de batedores
     * @throws RemoteException
     */
    public int getDroppedNumber() throws RemoteException;
    
    /**
     * Notifica todos os jogadores.
     * @param message
     */
	public void notifyAll(String message) throws RemoteException;;       
    
}