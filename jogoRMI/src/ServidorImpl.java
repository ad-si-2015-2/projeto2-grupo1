
/**
 * Classe ServidorImpl
 * Classe que implemente o servidor do jogo.
 * @author Fabio Junior e Douglas
 * @since 15/12/2015
 * @version 2.0
 */

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class ServidorImpl implements Servidor {
	
	private ArrayList<Cliente> clients;				//Lista de Clientes
    private Baralho deck;							//Baralho
    private String discard;							//Carta descartada
    private int playersNumber = 0;					//Numero de jogadores    
    private int turnPlayer = 0;						//Jogador da vez   
    private int status = 0;							//Status do jogo (valor 1, 2 ou 3)
    private int droppedNumber = 0;					//Numero de batedores
	
    
    public ServidorImpl(){    	
    	clients = new ArrayList<Cliente>();	
    }
       
  //Main
    public static void main(String[] args) {
    	
        try {
            ServidorImpl obj = new ServidorImpl();
            Servidor stub = (Servidor) UnicastRemoteObject.exportObject(obj, 0);		//Exporta obj para receber chamadas RMI 

            Registry registry = LocateRegistry.createRegistry(6789);
            registry.bind("Jogo", stub);

            System.out.println("Servidor iniciado!");

        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
    
	@Override
	public void registry(Cliente client) throws RemoteException {
		
		if(status == STATUS_JOGO_ATIVO){ 				//Verifica se o jogo ja comecou    		
        	client.notify(client.getName() + ", o jogo ja comecou. Aguarde um novo jogo!");
        	return;
    	} if(status == STATUS_JOGO_FINALIZADO){ 		//Verifica se o jogo ja acabou    		
        	client.notify(client.getName() + ", o jogo ja terminou. Aguarde um novo jogo!");
        	return;    		
    	} else if (clients.isEmpty() && client.getName().equals("Robot")){
    		System.out.println("O Robo nao pode ser o Game Manager");
    		return;
    	} else if (clients.isEmpty()) {					//Verifica se eh o primeiro jogador    		
    	
    		client.setID(0);
    		clients.add(client);
            client.notify(client.getName() + ", voce eh o Game Manager!\nDigite o numero de jogadores (3-13):");
            client.waitCommand();
            client.notify(client.getName() + ", escolha um comando:\n" + 
            		"/iniciar -> inicia o jogo\n/listar -> exibe o nome dos jogadores");            
        	client.waitCommand();
        } else if (!clients.isEmpty() && this.playersNumber == 0){	//Verifica se o game manager ja definiu o numero de jogadores
        	client.notify(client.getName() + ", espera o Game Manager definir o numero de jogadores!");
        	return;
        } else if (clients.size() == this.playersNumber){				//Verifica se atingiu o maximo de jogadores    		
        	client.notify(client.getName() + ", o jogo atingiu o numero maximo de jogadores. Aguarde um novo jogo!");
        	return;
        } else {										//Outro jogador valido        	
        	client.setID(clients.size());
        	clients.add(client);
        	notifyAll(clients.size() + " de " + playersNumber + " jogadores ja estao conectados!");
        	notifyAll("Aguarde o Game Manager iniciar o jogo!");
        }

	}

	@Override
	public int getStatus() throws RemoteException {
		return this.status;
	}

	@Override
	public void setStatus(int status) throws RemoteException {
		this.status = status;
	}

	@Override
	public void runCommand(String command) throws RemoteException {
		
		if (command.equals("/iniciar")) {					//Iniciar o jogo
            if (playersNumber > clients.size()) {
                notifyAll("Aguardando a entrada de jogadores. " + clients.size() + " de " + getPlayersNumber() + " conectados.");
            } else {
            	
                status = STATUS_JOGO_ATIVO;					//Jogo em andamento
                deck = new Baralho(playersNumber);			//Inicializa o baralho
                dealCards();								//Distribui as cartas
                for (Cliente client:clients) {
                    client.play();
                }
            }
        } else if (command.equals("/listar")) {
            
        	String message = "+----- JOGADORES -----+\n";
            for (int i = 0; i < clients.size(); i++) {
                message += "Jogador " + (i+1) + " - " + clients.get(i).getName() + "\n";
            }
            message += "+---------------------+";
            clients.get(0).notify(message);					//Notifica o game manager
        } else {
            clients.get(0).notify("Comando invalido! Tente novamente.");
        }
	}

	@Override
	public int getPlayersNumber() throws RemoteException {
		return this.playersNumber;
	}

	@Override
	public void setPlayersNumber(int number) throws RemoteException {
		
		if (number <= 13 && number >= 3) {
            this.playersNumber = number;
        }else{
            this.playersNumber = 3;
        }
	}

	@Override
	public void dealCards() throws RemoteException {
		
		notifyAll("Distribuindo cartas...");
    	for (Cliente client:clients){
    		for (int j=1; j<=4;j++){		//4 cartas para cada
    			client.addCard(deck.getCard());
    		}
    	}
    	setDiscard(deck.getCard());
	}

	@Override
	public int getTurnPlayer() throws RemoteException {
		return this.turnPlayer;
	}

	@Override
	public void setTurnPlayer(int playerID) throws RemoteException {
    	turnPlayer = playerID;
	}

	@Override
	public String getDiscard() throws RemoteException {
		return this.discard;
	}

	@Override
	public void setDiscard(String discard) throws RemoteException {
		this.discard = discard;
	}

	@Override
	public void setWinner(int playerID) throws RemoteException {
		
		this.status = STATUS_JOGO_AGUARDANDO;    	
    	notifyAll("O jogador " + clients.get(playerID).getName() + " venceu o jogo. Batedores em suas posicoes!");
	}

	@Override
	public void setLoser(int playerID) throws RemoteException {
		this.status = STATUS_JOGO_FINALIZADO;
    	notifyAll("O jogador " + clients.get(playerID).getName() + " perdeu o jogo. Jogo finalizado!");
	}

	@Override
	public void increaseDroppedNumber() throws RemoteException {
		this.droppedNumber++;
	}

	@Override
	public int getDroppedNumber() throws RemoteException {
		return this.droppedNumber;
	}

	@Override
	public void notifyAll(String message) throws RemoteException {
		for (Cliente client:clients){
    		client.notify(message);
    	}
	}

}