
/**
 * Classe ClienteImpl
 * Classe que implementa o cliente do jogo (jogador).
 * @author Fabio Junior e Douglas
 * @since 15/12/2015
 * @version 2.0
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Random;

public class ClienteImpl implements Cliente {
	
	private static Registry registry;
    private static Servidor server;				//Servidor do jogo
    private static BufferedReader reader;		//Buffer de leitura de entrada de dados
    private ArrayList<String> pocketCards;		//Cartas da mao do jogador
    private String name;						//Nome do jogador
    private int id;								//Identificador do jogador
    private boolean dropped = false;			//Verifica se o jogador bateu
	
    static {
        try {
            registry = LocateRegistry.getRegistry("localhost", 6789);
            server = (Servidor) registry.lookup("Jogo");

        } catch (RemoteException ex) {
            System.err.println("Nao foi possivel conectar ao servidor. Verifique se o servidor estah ativo.");
            System.err.println(ex);
            System.exit(0);
        } catch (NotBoundException ex) {
            System.err.println("Nao foi possivel encontrar o registro. Verifique se a variavel estah definida corretamente.");
            System.err.println(ex);
            System.exit(0);
        }
    }
    
    public ClienteImpl(){
    	
    	reader = new BufferedReader(new InputStreamReader(System.in));
    	pocketCards = new ArrayList<String>();    	
    }
    
	public static void main(String[] args) throws Exception {
						
    	ClienteImpl client = new ClienteImpl();
    	
        System.out.println("Informe seu nome:");        
        client.setName(reader.readLine());
        
        Cliente stub = (Cliente) UnicastRemoteObject.exportObject(client, 0);
        Random gerador = new Random();
        Registry registry = LocateRegistry.createRegistry(gerador.nextInt(1000) + 6000);
        registry.bind("Cliente", stub);
        server.registry(client);
    }
	
	@Override
	public String getName() throws RemoteException {
		return this.name;
	}

	@Override
	public void setName(String name) throws RemoteException {
		this.name = name;
	}

	@Override
	public void setID(int id) throws RemoteException {
		this.id = id;
	}

	@Override
	public void notify(String message) throws RemoteException {
		System.out.println(message);
	}

	@Override
	public void waitCommand() throws RemoteException {
		
		String command;
        try {
        	if(server.getPlayersNumber() == 0){		//Jogo nao iniciado e jogador eh o game manager
        		
        		command = reader.readLine();
        		int number = Integer.parseInt(command);        		
                server.setPlayersNumber(number);
                server.notifyAll("Numero de jogadores definido com sucesso: " + server.getPlayersNumber());
        	} else{
        		while (true) {
                    command = reader.readLine();
                    server.runCommand(command);
                    
                    if (command.equals("/iniciar") && server.getStatus() == 2) {
                        break;
                    }
                }
        	}
        } catch (NumberFormatException ex) {
        	System.out.println("Voce nao digitou um numero valido. Tente novamente!");
        	waitCommand();
        } catch (Exception ex) {
        	System.out.println("Erro na execucao do comando:" + ex.getMessage());
        }
	}

	@Override
	public void play() throws RemoteException {
		
		new Thread() {
            
    		@Override
            public void run() {
                
    			try {                   	
    				String answer = "";
                    System.out.println("O jogo foi iniciado!\nSuas cartas sao:\n");
                    printPocketCards();
                    
                    verifyWinner();		//Verifica se alguem saiu com o jogo ganho                    
                    
                    while (server.getStatus() == Servidor.STATUS_JOGO_ATIVO) {
                    	
                    	try {
                            if (id == server.getTurnPlayer()) {		//Verifica qual jogador eh a vez
                                
                            	System.out.println("Sua vez de jogar!");
                            	int discardIndex = -1;					//Indice da carta a ser descartada 
                                	
                            	if (server.getDiscard().equals("#")){
                            		System.out.println("Azar! A carta descartada eh o curinga. Voce deve mante-la por uma rodada.\n"+
                            				"Digite o indice da carta a ser trocada (1-4):");
                            		printPocketCards();
                            		discardIndex = getIndex();                            		
                            	} else{                      	
                            	
                            		System.out.println("A carta de descarte eh: " + server.getDiscard() + 
                            				"\nDigite /acc para aceitar o descarte ou /pass para passar a vez.");
                            		discardIndex = getAnswer();
                                }
                            	
                            	if (discardIndex != -1){                            		
                            		pocketCards.add(server.getDiscard());
                            		server.setDiscard(pocketCards.remove(discardIndex));
                            		System.out.println("Suas cartas agora sao:");
                            		printPocketCards();
                            	}
                            	
                            	if(!verifyWinner()){ //Define novo jogador da vez                       
                            		if (id+1 == server.getPlayersNumber()) server.setTurnPlayer(0);
                            		else server.setTurnPlayer((id+1));                            		
                            	}
                            	
                            }else{
                            	sleep(1000);                            	
                            }                
                            	
                        } catch (IOException ex) {
                        }
                    }

                    while (server.getStatus() == Servidor.STATUS_JOGO_AGUARDANDO) {
                        
                    	if (!dropped) {				//Para os jogadores que nao bateram
                            
                    		System.out.println("ATENCAO JOGADORES QUE NAO GANHARAM! Digitem /bater para nao perder! O ultimo sera o perdedor!");
                            answer = reader.readLine();

                            if (answer.equals("/bater")) {                        	
                                
                                if (server.getDroppedNumber() == server.getPlayersNumber()-1) {			//Ultimo jogador a bater                                    
                                	
                                	System.out.println("Voce foi o ultimo a bater! Voce PERDEU o jogo!");
                                    server.setLoser(id);
                                } else {
                                	dropped = true;
                                    server.increaseDroppedNumber();
                                    System.out.println("Voce bateu!");
                                }
                            }
                        } else {
                            System.out.println("Aguarde o fim do jogo!");
                            reader.readLine();
                        }
                    }
                } catch (Exception ex) {
                }
            }

        }.start();

	}

	@Override
	public void addCard(String card) throws RemoteException {
		pocketCards.add(card);
	}

	@Override
	public void printPocketCards() throws RemoteException {
		for (int i=0;i<pocketCards.size();i++){
    		System.out.println("Carta "+ (i+1) +" - " + pocketCards.get(i));            	
    	} 
	}

	@Override
	public boolean verifyWinner() {
		
		boolean winner = true;
		try{
	    	int index = Integer.parseInt(pocketCards.get(0).substring(0, pocketCards.get(0).length()-1));
	    	
	    	for(String card:pocketCards){
	    		if(Integer.parseInt(card.substring(0, card.length()-1)) != index) winner = false;
	    	}
	    	
	    	if (winner){
	    		this.dropped = true;
	    		server.increaseDroppedNumber();
	    		server.setWinner(this.id); 
	    	}
	    	return winner;
		}catch(NumberFormatException e){
			return false;
		}catch(IOException ex){
			return false;
		}
	}
	
	/**
     * Obtem o indice da carta da mao do jogador a ser descartada.
     * @return indice da carta da mao do jogador a ser descartada
     * @throws IOException
     */
    private int getIndex() throws IOException{
    	
    	int discardIndex = 0;
    	
		try{
			discardIndex = Integer.parseInt(reader.readLine())-1;
		}catch (NumberFormatException ex) {
        	System.out.println("Voce nao digitou um numero valido. Tente novamente!");  
        	discardIndex = getIndex();
        }
		
		if (discardIndex < 0 || discardIndex > 3){
			System.out.println("Indice invalido. O numero deve ser entre 1 e 4! Digite novamente:");
			discardIndex = getIndex();
		}
    	
    	return discardIndex;
    }
    
    /**
     * Obtem a resposta do usuario. Valida a resposta e retorna o indice da carta a ser descarta ou -1 caso nenhuma carta seja descartada.
     * @return indice da carta a ser descartada ou -1.
     * @throws IOException
     */
    private int getAnswer() throws IOException{
    	
    	String answer = reader.readLine();
    	int discardIndex = -1;
		
		if (answer.equals("/acc")) {	//Descarte aceito
			System.out.println("Descarte aceito. Digite o indice da carta a ser descartada (1-4):");                                    
			printPocketCards();
			discardIndex = getIndex();
		}else if(answer.equals("/pass")){
			System.out.println("Voce passou a vez. Aguarda a proxima rodada!");
		}else{
			System.out.println("Comando invalido. Tente novamente!");
			discardIndex = getAnswer();
		}
		return discardIndex;
    }

}