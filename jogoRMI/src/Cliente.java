
/**
 * Interface Cliente
 * Interface que define o cliente do jogo (jogador).
 * @author Fabio Junior e Douglas
 * @since 15/12/2015
 * @version 2.0
 */

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Cliente extends Remote {
	
	/**
     * Obtem o nome do jogador.
     * @return nome do jogador
     * @throws RemoteException
     */
    public String getName() throws RemoteException;					
    
    /**
     * Define o nome do Jogador.
     * @param name
     * @throws RemoteException
     */
    public void setName(String name) throws RemoteException;		
    
    /**
     * Define o identificador do cliente.
     * @param id
     * @throws RemoteException
     */
    public void setID(int id) throws RemoteException;				
	
	/**
	 * Envia mensagem ao jogador.
	 * @param message
	 * @throws RemoteException
	 */
    public void notify(String message) throws RemoteException;		
    
    /**
     * Espera por uma entrada do jogador.
     * @throws RemoteException
     */
    public void waitCommand() throws RemoteException;				   
    
    /**
     * Inicializa o jogo para este jogador.	
     * @throws RemoteException
     */
    public void play() throws RemoteException;					
    
    /**
     * Adiciona carta na mao do jogador.
     * @param card
     * @throws RemoteException
     */
    public void addCard(String card) throws RemoteException;	
    
    /**
     * Imprime as cartas da mao do jogador.
     * @throws RemoteException
     */
    public void printPocketCards() throws RemoteException;		
    
    /**
     * Verifica se o jogador ganhou o jogo
     * @return True or False
     * @throws RemoteException
     */
    public boolean verifyWinner() throws RemoteException; 
   
}
