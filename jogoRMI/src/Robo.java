
/**
 * Classe Robo
 * Classe do jogador automatico.
 * @author Fabio Junior
 * @since 26/01/2015
 * @version 1.0
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Robo implements Cliente {

	private static Registry registry;
    private static Servidor server;				//Servidor do jogo
    private static BufferedReader reader;		//Buffer de leitura de entrada de dados
    private ArrayList<String> pocketCards;		//Cartas da mao do jogador
    private String name;						//Nome do jogador
    private int id;								//Identificador do jogador
    private boolean dropped = false;			//Verifica se o jogador bateu
	
    static {
        try {
            registry = LocateRegistry.getRegistry("localhost", 6789);
            server = (Servidor) registry.lookup("Jogo");

        } catch (RemoteException ex) {
            System.err.println("Nao foi possivel conectar ao servidor. Verifique se o servidor estah ativo.");
            System.err.println(ex);
            System.exit(0);
        } catch (NotBoundException ex) {
            System.err.println("Nao foi possivel encontrar o registro. Verifique se a variavel estah definida corretamente.");
            System.err.println(ex);
            System.exit(0);
        }
    }
    
    public Robo(){
    	reader = new BufferedReader(new InputStreamReader(System.in));
    	pocketCards = new ArrayList<String>();    	
    }
    
	public static void main(String[] args) throws Exception {
						
    	Robo robot = new Robo();    	      
        robot.setName("Robot");
        
        Cliente stub = (Cliente) UnicastRemoteObject.exportObject(robot, 0);
        Random gerador = new Random();
        Registry registry = LocateRegistry.createRegistry(gerador.nextInt(1000) + 6000);
        registry.bind("Robo", stub);
        server.registry(robot);
    }
	
	@Override
	public String getName() throws RemoteException {
		return this.name;
	}

	@Override
	public void setName(String name) throws RemoteException {
		this.name = name;
	}

	@Override
	public void setID(int id) throws RemoteException {
		this.id = id;
	}

	@Override
	public void notify(String message) throws RemoteException {
		System.out.println(message);
	}

	@Override
	public void waitCommand() throws RemoteException {
		//Metodo nao usado
	}

	@Override
	public void play() throws RemoteException {
		
		new Thread() {
            
    		@Override
            public void run() {
                
    			try {                   	
    				String answer = "";
                    System.out.println("O jogo foi iniciado!\nSuas cartas sao:\n");
                    printPocketCards();                    
                    
                    verifyWinner();		//Verifica se alguem saiu com o jogo ganho                    
                    
                    while (server.getStatus() == Servidor.STATUS_JOGO_ATIVO) {
                    	
                    	try {
                            if (id == server.getTurnPlayer()) {		//Verifica qual jogador eh a vez
                                
                            	System.out.println("Sua vez de jogar!");
                            	int discardIndex = -1;					//Indice da carta a ser descartada 
                                	
                            	if (server.getDiscard().equals("#")){
                            		System.out.println("Azar! A carta descartada eh o curinga. Voce deve mante-la por uma rodada.\n"+
                            				"Digite o indice da carta a ser trocada (1-4):");
                            		printPocketCards();
                            		discardIndex = chooseDiscard("#");                            		
                            	} else{                      	
                            	
                            		System.out.println("A carta de descarte eh: " + server.getDiscard());
                            		discardIndex = chooseDiscard(server.getDiscard());
                                }
                            	
                            	if (discardIndex != -1){                            		
                            		pocketCards.add(server.getDiscard());
                            		server.setDiscard(pocketCards.remove(discardIndex));
                            		System.out.println("Voc� aceitou o descarte!");
                            		System.out.println("Suas cartas agora sao:");
                            		printPocketCards();
                            	}else{
                            		System.out.println("Voce passou a vez!");
                            	}
                            	
                            	if(!verifyWinner()){ //Define novo jogador da vez                       
                            		if (id+1 == server.getPlayersNumber()) server.setTurnPlayer(0);
                            		else server.setTurnPlayer((id+1));                            		
                            	}
                            	
                            }else{
                            	sleep(1000);                            	
                            }                
                            	
                        } catch (IOException ex) {
                        }
                    }

                    while (server.getStatus() == Servidor.STATUS_JOGO_AGUARDANDO) {
                        
                    	if (!dropped) {				//Para os jogadores que nao bateram
                            
                    		System.out.println("ATENCAO JOGADORES QUE NAO GANHARAM! Digitem /bater para nao perder! O ultimo sera o perdedor!");
                            answer = "/bater";

                            if (answer.equals("/bater")) {                        	
                                
                                if (server.getDroppedNumber() == server.getPlayersNumber()-1) {			//Ultimo jogador a bater                                    
                                	
                                	System.out.println("Voce foi o ultimo a bater! Voce PERDEU o jogo!");
                                    server.setLoser(id);
                                } else {
                                	dropped = true;
                                    server.increaseDroppedNumber();
                                    System.out.println("Voce bateu!");
                                }
                            }
                        } else {
                            System.out.println("Aguarde o fim do jogo!");
                            reader.readLine();
                        }
                    }
                } catch (Exception ex) {
                }
            }

        }.start();
	}
		
	@Override
	public void addCard(String card) throws RemoteException {
		pocketCards.add(card);
	}

	@Override
	public void printPocketCards() throws RemoteException {
		for (int i=0;i<pocketCards.size();i++){
    		System.out.println("Carta "+ (i+1) +" - " + pocketCards.get(i));            	
    	} 
	}

	@Override
	public boolean verifyWinner() {
		
		boolean winner = true;
		try{
	    	int index = Integer.parseInt(pocketCards.get(0).substring(0, pocketCards.get(0).length()-1));
	    	
	    	for(String card:pocketCards){
	    		if(Integer.parseInt(card.substring(0, card.length()-1)) != index) winner = false;
	    	}
	    	
	    	if (winner){
	    		this.dropped = true;
	    		server.increaseDroppedNumber();
	    		server.setWinner(this.id); 
	    	}
	    	return winner;
		}catch(NumberFormatException e){
			return false;
		}catch(IOException ex){
			return false;
		}
	}
	
	/**
	 * Decide o indice da carta a ser descartada ou -1
	 * @param card (carta de descarte)
	 * @return indice da carta a ser descartada ou -1
	 */
	public int chooseDiscard(String card){
				
		if(card.equals("#")){
			return analyse("");
		} else{
			if(pocketCards.contains("#")) return pocketCards.indexOf("#");			
			else return analyse(card);			
		}		
	}	
	
	public int analyse(String extraCard){
		
		int number = -1;
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		for(String card:pocketCards){
			list.add(Integer.parseInt(card.substring(0, card.length()-1)));
		}
		if (!extraCard.equals("")) list.add(Integer.parseInt(extraCard.substring(0,extraCard.length()-1)));
		Collections.sort(list);
		
		for(int i=0;i< list.size()-1;i++){
			if(list.get(i) != list.get(i+1)){
				if(i == 0) number = list.get(i);					
				else number = list.get(i+1);
				break;
			}
		}
		
		for(int i=0;i<4;i++){
			int num = Integer.parseInt(pocketCards.get(i).substring(0, pocketCards.get(i).length()-1));
			if (num == number) return i;
		}
		return -1;		
	}
	
}