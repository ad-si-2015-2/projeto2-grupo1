
/**
 * Classe Baralho
 * Classe desenvolvida para abstrair um baralho.
 * @author Fabio Junior
 * @since 09/01/2016
 * @version 2.0
 */

import java.util.ArrayList;
import java.util.Random;

public class Baralho{
	
	private ArrayList<String> deck;		//Baralho
	private int playersNumber;
	
	/**
	 * Inicializa o baralho de acordo com o numero de jogadores.
	 * @param playersNumber
	 */
	public Baralho(int playersNumber){	
		
		deck = new ArrayList<String>();
		this.playersNumber = playersNumber;
		startDeck();
	}
	
	/**
	 * Adiciona as cartas ao baralho.
	 * Uma carta possui um numero e um naipe. 
	 * O naipe eh definico por: O(ouros), E(espadas), C(copas) e P(paus). 
	 * O simbolo # define a carta curinga
	 */
	public void startDeck(){
		
		deck.clear();
		deck.add("#");
		
		for(int i=1;i<=playersNumber;i++){

			deck.add(i + "O");
			deck.add(i + "E");
			deck.add(i + "C");
			deck.add(i + "P");			
		}		
	}
	
	/**
	 * Remove a carta do baralho e a retorna.
	 * @return uma carta aleatoria
	 */
	public String getCard(){		
		
		Random rd = new Random();		
		return deck.remove(rd.nextInt(deck.size()));
	}	
	
}